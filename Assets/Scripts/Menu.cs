﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public Canvas QuitMenu;
    public Button ButtonStart;
    public Button ButtonExit;
    private Canvas menuUI;

    private void Start()
    {
        menuUI = GetComponent<Canvas>();
        menuUI.enabled = true;
        ButtonStart = GetComponent<Button>();
        ButtonExit = GetComponent<Button>();
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            menuUI.enabled = !menuUI.enabled;
        }
    }

    public void ExitGameButton()
    {
        ButtonStart.enabled = false;
        ButtonExit.enabled = false;
        Application.Quit();
    }


    public void StartGameButton()
    {
        SceneManager.LoadScene("Scene");
    }

}
