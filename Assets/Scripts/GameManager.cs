﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private DateTime timeBegin;
    private TimeSpan timeTimer;
    public static string timeString { get; set; }
    public static bool isOnTimer = true;
    public static bool finished = false;
    public Rect rectTimer = new Rect(10, 100, 100, 20);
    public Rect rectRecordText = new Rect(10, 30, 100, 20);
    public Rect rectRecordValue = new Rect(10, 40, 100, 20);

    public Maze mazePrefab;

	public Player playerPrefab;

	private Maze mazeInstance;

	private Player playerInstance;

    private void Start() {
        /*PlayerPrefs.SetString("record", "");
        PlayerPrefs.SetString("start", "");*/
        Debug.Log(PlayerPrefs.GetString("time") + " Rec" + PlayerPrefs.GetString("record"));
        StartCoroutine(BeginGame());
        PlayerPrefs.SetString("time", "");
        timeBegin = DateTime.Now;
        
    }
	
	private void Update () {
        if (isOnTimer)
        {
            timeTimer = DateTime.Now - timeBegin;
            timeString = string.Format("{0:D2}:{1:D2}:{2:D2}", timeTimer.Hours, timeTimer.Minutes, timeTimer.Seconds);
        }
        if (Input.GetKeyDown(KeyCode.Space)||finished) {
            finished = false;
            isOnTimer = true;
            RestartGame();
		}
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
    }

    void OnGUI()
    {
        GUI.Label(rectTimer, timeString);
        GUI.Label(rectRecordText, "Your record:");
        GUI.Label(rectRecordValue, PlayerPrefs.GetString("record"));
    }

    private IEnumerator BeginGame () {
		Camera.main.clearFlags = CameraClearFlags.Skybox;
		Camera.main.rect = new Rect(0f, 0f, 1f, 1f);
		mazeInstance = Instantiate(mazePrefab) as Maze;
		yield return StartCoroutine(mazeInstance.Generate());
		playerInstance = Instantiate(playerPrefab) as Player;
        /*playerInstance.TeleportToCell(
        mazeInstance.GetCell(mazeInstance.CenterCoordinates));*/
        playerInstance.TeleportToCell(mazeInstance.GetCell(mazeInstance.RandomCoordinates));
        //Debug.Log(playerInstance.targetPosition);
        
		Camera.main.clearFlags = CameraClearFlags.Depth;
		Camera.main.rect = new Rect(0f, 0f, 0.5f, 0.5f);
		Color ambientColor = new Color();
		ambientColor.r = 0.5f;
		ambientColor.g = 0.5f;
		ambientColor.b = 0.5f;
		RenderSettings.ambientLight = ambientColor;
    }

	private void RestartGame () {
        if (isOnTimer)
        {
            if(PlayerPrefs.GetString("time") != ""&& PlayerPrefs.GetString("record") != "")
            {
                Debug.Log(PlayerPrefs.GetString("time"));
                if (DateTime.Compare(DateTime.Parse(PlayerPrefs.GetString("time")), DateTime.Parse(PlayerPrefs.GetString("record"))) < 0)
                {
                    PlayerPrefs.SetString("record", PlayerPrefs.GetString("time"));
                }
            }else if(PlayerPrefs.GetString("record")==""&& PlayerPrefs.GetString("time") != "")
            {
                PlayerPrefs.SetString("record", PlayerPrefs.GetString("time"));
            }


        }
        timeBegin = DateTime.Now;
        StopAllCoroutines();
		Destroy(mazeInstance.gameObject);
		if (playerInstance != null) {
			Destroy(playerInstance.gameObject);
		}
		StartCoroutine(BeginGame());
	}
}