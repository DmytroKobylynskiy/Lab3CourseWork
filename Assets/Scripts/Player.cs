﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{   
    public Rect windowRect = new Rect(20, 20, 120, 50);
    public bool isOpen = false;
    public static string time;

	private const float runSpeedMultiplier = 2.0f;
	private const float smallDelta = 0.01f;
	private const float mediumDelta = 0.1f;

	private GameObject model;               
	private Actions actions;                
	private MazeDirection lookDirection;    
	public Vector3 targetPosition;         
	private bool isRunning = false;         
	private Rigidbody player_rigidbody;     

	private ArrayList route = new ArrayList (); 
	public float turnSmoothing = 15f;
	public float dampingTime = 0.1f;
	public string modelName = "SportyGirl";

	public MazeCell GetCurrentCell()
	{   
		if(0 < route.Count) {
			return (MazeCell)route[0];
		}
		return null;
	}

	public MazeCell GetDestinationCell()
	{   
		if(0 < route.Count) {
			return (MazeCell)route[route.Count - 1];
		}
		return null;
	}

	public void TeleportToCell (MazeCell cell)
	{   
		if (GetCurrentCell() != null) {
			GetCurrentCell().OnPlayerExited ();
		}
		route.Clear(); 
		route.Add(cell);
		transform.position = cell.transform.position;
		targetPosition = transform.position;
		GetCurrentCell().OnPlayerEntered ();
	}

	public void MoveToCell (MazeCell cell)
	{   
		route.Add (cell);
	}

	public void MoveDirection (MazeDirection direction)
	{   
		MazeCell cellToMoveFrom = GetDestinationCell();
		MazeCellEdge edge = cellToMoveFrom.GetEdge (direction);
		//Debug.Log(string.Format("Move:{0} from {1} to {2}", direction, cellToMoveFrom.name, edge.otherCell.name));
		if (edge is MazePassage) {
			if (cellToMoveFrom == edge.otherCell) {
				MoveToCell (edge.cell);
			} else if (cellToMoveFrom == edge.cell) {
				MoveToCell (edge.otherCell);
			} else {
				Debug.LogError (string.Format ("Move:{0} from {1} to {2}", direction, cellToMoveFrom.name, edge.otherCell.name));
			}
		}
	}

	public void LookDirection (MazeDirection direction)
	{  
		lookDirection = direction;
	}

	void Awake ()
	{ 
		model = transform.FindChild (modelName).gameObject;
		actions = model.GetComponent<Actions> ();
		targetPosition = transform.position;
		player_rigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate ()
	{
		if (1 < route.Count) {
			float deltaToNextCell = Mathf.Max (
				                        Mathf.Abs (targetPosition.x - transform.position.x),
				                        Mathf.Abs (targetPosition.z - transform.position.z));
			if (0.4f > deltaToNextCell) {
				if (GetCurrentCell() != null) {
					GetCurrentCell().OnPlayerExited ();
				}
				route.RemoveAt (0);
				GetCurrentCell().OnPlayerEntered ();
				targetPosition = GetCurrentCell().transform.position;
			}
		}

		float delta = Mathf.Max (Mathf.Abs (targetPosition.x - transform.position.x),
			              Mathf.Abs (targetPosition.z - transform.position.z));
		if (delta > smallDelta) {
			
			Vector3 directionToTargetPosition = 
				targetPosition - transform.position;

			
			Quaternion targetRotation = Quaternion.LookRotation (
				directionToTargetPosition, Vector3.up);
			Quaternion newRotation = Quaternion.Lerp (player_rigidbody.rotation,
				                         targetRotation, turnSmoothing * Time.deltaTime);
			player_rigidbody.MoveRotation (newRotation);

			if (2 < route.Count) {  
				const string message = "Run";
				actions.SendMessage (message, SendMessageOptions.DontRequireReceiver);
				isRunning = true;
			} else {   
				if (1 == route.Count && delta > 0.2f) {
					const string message = "Walk";
					actions.SendMessage (message, SendMessageOptions.DontRequireReceiver);
					isRunning = false;
				}
			}

			if (isRunning) {
				player_rigidbody.MovePosition (transform.position +
				transform.forward * Time.deltaTime * runSpeedMultiplier);
			} else {
				player_rigidbody.MovePosition (
					transform.position + transform.forward * Time.deltaTime);
			}
		} else {
			const string message = "Stay";
			actions.SendMessage (message, SendMessageOptions.DontRequireReceiver);
			float angle = Quaternion.Angle (transform.rotation, lookDirection.ToRotation ());
			if (Mathf.Abs (angle) > mediumDelta) {
				Rigidbody player_rigidbody = GetComponent<Rigidbody> ();
				Quaternion newRotation = Quaternion.Lerp (player_rigidbody.rotation,
					lookDirection.ToRotation (), turnSmoothing * Time.deltaTime);
				player_rigidbody.MoveRotation (newRotation);
			}
		}
	}

	private void Update ()
	{
        if (Input.GetKeyDown (KeyCode.W) || Input.GetKeyDown (KeyCode.UpArrow)) {
			MoveDirection (lookDirection);
			LookDirection (lookDirection);
		} else if (Input.GetKeyDown (KeyCode.D) || Input.GetKeyDown (KeyCode.RightArrow)) {
			MazeDirection direction = lookDirection.GetNextClockwise ();
			MoveDirection (direction);
			LookDirection (direction);
		} else if (Input.GetKeyDown (KeyCode.S) || Input.GetKeyDown (KeyCode.DownArrow)) {
			MazeDirection direction = lookDirection.GetOpposite ();
			MoveDirection (direction);
			LookDirection (direction);
		} else if (Input.GetKeyDown (KeyCode.A) || Input.GetKeyDown (KeyCode.LeftArrow)) {
			MazeDirection direction = lookDirection.GetNextCounterclockwise ();
			MoveDirection (direction);
			LookDirection (direction);
		} else if (Input.GetKeyDown (KeyCode.Q)) {
			LookDirection (lookDirection.GetNextCounterclockwise ());
		} else if (Input.GetKeyDown (KeyCode.E)) {
			LookDirection (lookDirection.GetNextClockwise ());
        }else if (targetPosition.y > 1)
        {
            GameManager.isOnTimer = false;
            time = GameManager.timeString;
            isOpen = true;
            
        }
    }


    void OnGUI() 
    {
        if (isOpen) 
        {
            GUI.Box(new Rect(400, 200, 200, 120), "You won! Your time :\n"+ time + ". One more game?");
            if (GUI.Button(new Rect(400, 250, 100, 50), "Yes")) 
            {
                GameManager.finished = true;
                PlayerPrefs.SetString("time", time);
                Debug.Log(time);
                isOpen = false;
                SceneManager.LoadScene("Scene");
            }
            if (GUI.Button(new Rect(500, 250, 100, 50), "No")) 
            {
                isOpen = false;
                GameManager.finished = true;
                SceneManager.LoadScene("Menu");
            }
        }
    }

}